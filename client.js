function writeToChatList(message, from) {
    var field = document.querySelector("#messages"),
        line = document.createElement('div');
    line.classList.add('mess');
    if (from) {
        var b = document.createElement('b');
        b.innerHTML = from;
        line.appendChild(b);
    }
    var span = document.createElement('span');
    span.innerHTML = message;
    line.appendChild(span);
    field.appendChild(line);
}

var UserName = '';
while (UserName.length == 0) {
    UserName = prompt('Введите Ваше имя');
    UserName.trim();
}


var ws = new WebSocket("ws://127.0.0.1:5555");
ws.onopen = function() {
    var sendM = {
        "action": "login",
        "data": {
            "name": UserName
        }
    };
    ws.send(JSON.stringify(sendM));
    document.querySelector("#sender").addEventListener("click", function(e) {
        e.preventDefault();
        var text = document.querySelector("#message").value.trim();
        if (text.length > 0) {
            //#1 - вывести в чат
            writeToChatList(text, "Я");
            //#2 - отправить сообщение на сервер
            var sendM = {
                action: "message",
                data: {
                    name: UserName,
                    text: text
                }
            };
            ws.send(JSON.stringify(sendM));
            //#3 - очистить строку
            document.querySelector("#message").value = '';
        }
    });
}
ws.onmessage = function(message) {
    var input = JSON.parse(message.data);
    writeToChatList(input.text, input.from);
}

//я вошел
//я написал