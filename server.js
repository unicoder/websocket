var ws = require('ws'),
    wss = ws.Server;
var server = new wss({
    host: "127.0.0.1",
    port: 5555
});
var users = new Set();

var sendMessage = function(from, message) {
    for (var user of users) {
        if (user !== from) {
            user.send(message);
        }
    }
}

server.on("listening", function() {
    console.log("Мы запустились 5555");
});

server.on("connection", function(thisUser) {
    users.add(thisUser);
    console.log('К нам постучался пользователь');
    thisUser.on("message", function(message) {
        console.log("Пользователь написал " + message);
        var input = JSON.parse(message);
        if (typeof input.action) {
            switch (input.action) {
                case "login":
                    sendMessage(
                        thisUser,
                        JSON.stringify({
                            "from": "Система",
                            "text": "К нам присоединился " + input.data.name
                        })
                    );
                    break;
                case "message":
                    sendMessage(
                        thisUser,
                        JSON.stringify({
                            "from": input.data.name,
                            "text": input.data.text
                        })
                    );
                    break;
            }
        }


    });
    thisUser.on("close", function() {
        console.log('Пользователь ушел');
        users.delete(thisUser);
    });
});
``